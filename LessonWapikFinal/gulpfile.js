var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function() {
    return gulp.src('app/styles/*.scss')
        .pipe(sass())
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
        .pipe(gulp.dest('app/styles'))
});

gulp.task('css-libs', function() {
   return gulp.src('app/styles/*.css')
   .pipe(cssnano())
   .pipe(rename({suffix: '.min'}))
   .pipe(gulp.dest('app/styles'));
});

